#!/usr/bin/env python3
# coding: utf-8
"""
    programme de gestion des services et VNF sur ONAP
"""
from flask import Flask
from newportal.views import home
from newportal.views import customers
from newportal.views import resources
from newportal.views import services
from newportal.views import vendors
from newportal.views import softwares
from newportal.views import service_instances
from newportal.views import vnf_instances
from newportal.views import vf_modules
from newportal.views import cloud_regions
from newportal.views import tenants
from newportal.views import sdnc_preloads
from newportal.views import nbi
from newportal.views import services_nbi
from newportal.views import virtual_link_instances
from newportal.views import onboard_services
from newportal.views import notifications

MY_WEB_SERVER = Flask(__name__)
MY_WEB_SERVER.register_blueprint(home.MOD)
MY_WEB_SERVER.register_blueprint(customers.MOD)
MY_WEB_SERVER.register_blueprint(resources.MOD)
MY_WEB_SERVER.register_blueprint(services.MOD)
MY_WEB_SERVER.register_blueprint(vendors.MOD)
MY_WEB_SERVER.register_blueprint(softwares.MOD)
MY_WEB_SERVER.register_blueprint(service_instances.MOD)
MY_WEB_SERVER.register_blueprint(vnf_instances.MOD)
MY_WEB_SERVER.register_blueprint(vf_modules.MOD)
MY_WEB_SERVER.register_blueprint(cloud_regions.MOD)
MY_WEB_SERVER.register_blueprint(tenants.MOD)
MY_WEB_SERVER.register_blueprint(sdnc_preloads.MOD)
MY_WEB_SERVER.register_blueprint(nbi.MOD)
MY_WEB_SERVER.register_blueprint(services_nbi.MOD)
MY_WEB_SERVER.register_blueprint(virtual_link_instances.MOD)
MY_WEB_SERVER.register_blueprint(onboard_services.MOD)
MY_WEB_SERVER.register_blueprint(notifications.MOD)
