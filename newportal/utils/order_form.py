#!/usr/bin/env python
# coding: utf-8
"""
    programme de gestion des services et VNF sur ONAP
"""
from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import DataRequired


class OrderForm(Form):
    """
    Service ID form
    """
    service_id = StringField('Service ID',
                             validators=[DataRequired()])
