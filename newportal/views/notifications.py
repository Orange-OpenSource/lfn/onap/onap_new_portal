#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
"""
    notifications management
"""
import time
from datetime import datetime
import requests
from flask import Blueprint, render_template
import newportal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()
# pylint: disable=inconsistent-return-statements

MOD = Blueprint('notifications', __name__, url_prefix='/notifications',
                static_folder='../static')
LISTENER_URL = newportal_utils.get_config("onap.listener.url")
URL_NOTIFICATIONS = newportal_utils.get_config("onap.listener.list_notif\
ication_url")
URL1 = LISTENER_URL + URL_NOTIFICATIONS

HEADERS_LISTENER = newportal_utils.get_config("onap.listener.headers")

PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/notification_list/', methods=['GET'])
def notification_list():
    """
    menu de gestion des notifications
    GET = affichage de la liste des notifications
    """
    try:
        response = requests.request(
            "GET",
            URL1,
            headers=HEADERS_LISTENER,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        notifications = response
        old_notifs = []
        new_notifs = []
        for event in notifications:
            # print(round(time.time()))
            # print(round(event['eventDate']/1000))
            # print(round(time.time()) - round(event['eventDate']/1000))
            ecart = round(time.time()) - round(event['eventDate']/1000)
            event['eventDate'] = time.strftime('%d-%m-%Y %H:%M:%S',
                                               time.localtime(round(event['ev\
entDate']/1000)))
            if ecart > 300:
                old_notifs.append(event)
            else:
                new_notifs.append(event)
        return render_template(
            'notifications/notification_list.html',
            old_notifications=old_notifs,
            new_notifications=new_notifs,
            current_time=datetime.now().strftime('%d-%m-%Y %H:%M:%S'))
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to Listener : check rebond tunnel ? \
Listener running ?'
        return render_template(
            'exception_annexe.html',
            message=message)
