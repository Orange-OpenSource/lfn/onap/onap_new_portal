#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
#  pylint: disable=R1702
"""
    relations between customer/cfs/rfs instances
"""
import requests
from flask import Blueprint, render_template
import newportal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('relations', __name__, url_prefix='/relations',
                static_folder='../static')

BASE_SERVICE_API_URL = newportal_utils.get_config("bss_gw.service_api.url")
HEADERS = newportal_utils.get_config("bss_gw.service_api.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/relations_display', methods=['GET'])
def relations_display():
    """
    cfs and rfs instance management
    GET = display cfs / rfs instances with related customer
    """
    service_api_url = BASE_SERVICE_API_URL + "/service"
    try:
        response = requests.request(
            "GET",
            service_api_url,
            headers=HEADERS,
            proxies=PROXIES)
        services = response.json()
        cfs_list = []
        rfs_list = []
        customer_list = []
        customer_to_cfs_relation = {}
        customer_to_cfs_relations = []
        cfs_to_rfs_relation = {}
        cfs_to_rfs_relations = []
        for service in services:
            if service["category"] == "cfs":
                cfs_list.append(service)
                customer = service["relatedParty"][0]
                customer_list.append(customer)
                customer_to_cfs_relation["from"] = customer["id"]
                customer_to_cfs_relation["to"] = service["id"]
                customer_to_cfs_relations.append(customer_to_cfs_relation)
                customer_to_cfs_relation = {}
                for elem in service["supportingService"]:
                    cfs_to_rfs_relation["from"] = service["id"]
                    cfs_to_rfs_relation["to"] = elem["id"]
                    cfs_to_rfs_relations.append(cfs_to_rfs_relation)
                    cfs_to_rfs_relation = {}
            else:
                service["name"] = service["name"][:-30]
                rfs_list.append(service)
            customer_list = list({v['id']:v for v in customer_list}.values())
            cfs_list = list({v['id']:v for v in cfs_list}.values())
            rfs_list = list({v['id']:v for v in rfs_list}.values())
        return render_template(
            'relations/relations.html',
            cfs_list=cfs_list,
            rfs_list=rfs_list,
            customer_list=customer_list,
            customer_to_cfs_relations=customer_to_cfs_relations,
            cfs_to_rfs_relations=cfs_to_rfs_relations)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to Service API : check rebond tunnel'
        return render_template(
            'exception_annexe.html',
            message=message)
