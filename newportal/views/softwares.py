#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
"""
    software (VSP) management
"""
import requests
from flask import Blueprint, render_template
import newportal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('softwares', __name__, url_prefix='/softwares',
                static_folder='../static')
SDC_URL = newportal_utils.get_config("onap.sdc.url2")
URL_VSP = newportal_utils.get_config("onap.sdc.list_vsp_url")
HEADERS_SDC = newportal_utils.get_config("onap.sdc.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/software_list/', methods=['GET'])
def software_list():
    """
    menu de gestion des vendors
    GET = affichage de la liste des vendors
    """
    try:
        response = requests.request(
            "GET",
            SDC_URL + URL_VSP,
            headers=HEADERS_SDC,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        softwares = response['results']
        return render_template(
            'softwares/software_list.html',
            softwares=softwares)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception_annexe.html',
            message=message)
