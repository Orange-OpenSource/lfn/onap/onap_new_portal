#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
"""
    resources management
"""
import requests
from flask import Blueprint, render_template
import newportal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('resources', __name__, url_prefix='/resources',
                static_folder='../static')
SDC_URL = newportal_utils.get_config("onap.sdc.url")
URL_RESOURCES = newportal_utils.get_config("onap.sdc.url_resources_in_sdc")
HEADERS_SDC = newportal_utils.get_config("onap.sdc.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/vnf_list/', methods=['GET'])
def resource_list():
    """
    menu de gestion des resources de type VF
    GET = affichage de la liste des resources
    """
    try:
        response = requests.request(
            "GET",
            SDC_URL + URL_RESOURCES,
            headers=HEADERS_SDC,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        vnfs = []
        for resource in response:
            if resource['resourceType'] == 'VF':
                vnfs.append(resource)
        return render_template(
            'resources/vnf_list.html',
            vnfs=vnfs)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception.html',
            message=message)
