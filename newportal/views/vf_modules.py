#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
"""
    vf_modules management
"""
import requests
from flask import Blueprint, render_template, request, redirect
import newportal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('vf_modules', __name__, url_prefix='/vf_modules',
                static_folder='../static')
AAI_URL = newportal_utils.get_config("onap.aai.url")
URL_VNF_INSTANCES = newportal_utils.get_config("onap.aai.url_vnf_instances")
URL = AAI_URL + URL_VNF_INSTANCES
HEADERS_AAI = newportal_utils.get_config("onap.aai.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/vf_module_list/', methods=['GET'])
def vf_module_list():
    """
    vf_modules management
    GET = display vf_modules list with related customer
    """
    vfmodulelist2 = []
    try:
        response = requests.request(
            "GET",
            URL,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        vnfinstancelist = response['generic-vnf']
        for vnfinstance in vnfinstancelist:
            url2 = URL +\
                "/generic-vnf/" + str(vnfinstance['vnf-id']) +\
                "/vf-modules"
            response2 = requests.request(
                "GET",
                url2,
                headers=HEADERS_AAI,
                proxies=PROXIES,
                verify=False)
            response2 = response2.json()
            if 'vf-module' in response2.keys():
                vfmodulelist1 = response2['vf-module']
                # pour chaque vf_module on ajoute les champs vnf_name avec
                # et vnf_id
                for vf_module in vfmodulelist1:
                    vf_module['vnf-instance-name'] =\
                        vnfinstance['vnf-name']
                    vf_module['vnf-instance-id'] =\
                        vnfinstance['vnf-id']
                    vfmodulelist2.append(vf_module)
        return render_template(
            'vf_modules/vf_module_list.html',
            vf_module_list=vfmodulelist2)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/vf_module_delete', methods=['GET'])
def vf_module_delete():
    """
    vf_module management
    launch delete vf_module and display page after done
    """
    try:
        # vf_module_id = request.args.get('id')
        vnf_instance_id = request.args.get('vnf-instance-id')
        vf_module_id = request.args.get('vf-module-id')
        resource_version = request.args.get('resource-version')
        url2 = AAI_URL + "/aai/v13/network/generic-vnfs/generic-vnf/" +\
                         str(vnf_instance_id) +\
                         "/vf-modules/vf-module/" + str(vf_module_id) +\
                         "?resource-version=" + str(resource_version)
        requests.request(
            "DELETE",
            url2,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        redirect_url = ("/vf_modules/vf_module_delete_result")
        return redirect(redirect_url)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/vf_module_delete_result', methods=['GET'])
def vf_module_delete_result():
    """
    vf_module management
    launch delete vf_module and display page after done
    """
    try:
        return render_template(
            'vf_modules/vf_module_delete_result.html')
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)
