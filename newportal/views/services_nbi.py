#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
#  pylint: disable=R1702
"""
    services management
"""
import logging
import requests
import urllib3
from flask import Blueprint, render_template, request, redirect
import newportal.utils.utils as newportal_utils

import onap_tests.scenario.e2e as e2e
import onap_tests.utils.exceptions as onap_test_exceptions
import onap_tests.components.service as service


MOD = Blueprint('services_nbi', __name__, url_prefix='/services_nbi',
                static_folder='../static')
NBI_URL = newportal_utils.get_config("onap.nbi.url")
URL_SERVICE_CATALOG = newportal_utils.get_config(
    "onap.nbi.url_services_catalog")
URL_SERVICE_INVENTORY = newportal_utils.get_config(
    "onap.nbi.url_services_inventory")
URL_SERVICE_ORDER = newportal_utils.get_config(
    "onap.nbi.url_services_order")
HEADERS_NBI = newportal_utils.get_config("onap.nbi.headers")
AAI_URL = newportal_utils.get_config("onap.aai.url")
URL_CUSTOMERS = newportal_utils.get_config("onap.aai.list_customer_url")
URL = AAI_URL + URL_CUSTOMERS
HEADERS_AAI = newportal_utils.get_config("onap.aai.headers")
PROXIES = newportal_utils.get_config("general.proxy")

urllib3.disable_warnings()
URLLIB3_LOG = logging.getLogger("urllib3")
URLLIB3_LOG.setLevel(logging.CRITICAL)


@MOD.route('/services_nbi_list/', methods=['GET'])
def services_nbi_list():
    """
    services management
    GET = display service models via NBI
    """
    try:
        response = requests.request(
            "GET",
            NBI_URL + URL_SERVICE_CATALOG,
            headers=HEADERS_NBI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        services = response
        for elem in services:
            if elem['distributionStatus'] != "DISTRIBUTED":
                services.remove(elem)
        resultat = filter_versions(services)
        return render_template(
            'services_nbi/services_nbi_list.html',
            services=resultat)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


def filter_versions(elems):
    """
    filtering to display only last version
    """
    results = []
    for elem1 in elems:
        same_service_list = []
        for elem2 in elems:
            if elem2["name"] == elem1["name"]:
                same_service_list.append(elem2)
        seq = [x['version'] for x in same_service_list]
        max_version = max(seq)
        for elem3 in elems:
            if ((elem3["name"] == same_service_list[0]["name"]) and
                    (elem3["version"] == max_version)):
                results.append(elem3)
    results = list({v['id']: v for v in results}.values())
    return results


@MOD.route('/services_nbi_inventory/', methods=['GET'])
def services_nbi_inventory():
    """
    services instances management
    GET = display service instances via NBI
    """
    services_instances = []
    try:
        response = requests.request(
            "GET",
            URL,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        customers = response['customer']
        for customer in customers:
            response2 = requests.request(
                "GET",
                NBI_URL +
                URL_SERVICE_INVENTORY +
                '?relatedParty.id=' +
                customer['global-customer-id'],
                headers=HEADERS_NBI,
                proxies=PROXIES,
                verify=False)
            if response2.status_code == 200:
                response2 = response2.json()
                services_instances_tmp = response2
                for service_instance in services_instances_tmp:
                    services_instances.append(service_instance)
        return render_template(
            'services_nbi/services_instances_nbi_list.html',
            services_instances=services_instances)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/services_nbi_inventory_detail/', methods=['GET'])
def services_nbi_inventory_detail():
    """
    services instances management
    GET = display one service instance via NBI
    """
    try:
        service_order_id = request.args.get('service_order_id')
        response = requests.request(
            "GET",
            NBI_URL + URL_SERVICE_ORDER + "/" + service_order_id,
            headers=HEADERS_NBI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        service_order = response
        service_instance_id = service_order['orderItem'][0]['service']['id']
        service_id = service_order['orderItem'][0]['service']['\
serviceSpecification']['id']
        url = NBI_URL + URL_SERVICE_INVENTORY + \
            "/" + service_instance_id + \
            "?serviceSpecification.id=" + service_id
        response = requests.request(
            "GET",
            url,
            headers=HEADERS_NBI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        service_instance = response
        return render_template(
            'services_nbi/service_instance_nbi_detail.html',
            service_instance=service_instance)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/services_nbi_order_list/', methods=['GET'])
def services_nbi_order_list():
    """
    services management
    GET = get NBI service order
    """
    try:
        response = requests.request(
            "GET",
            NBI_URL + URL_SERVICE_ORDER,
            headers=HEADERS_NBI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        service_orders = response
        return render_template(
            'services_nbi/services_nbi_order_list.html',
            service_orders=service_orders)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/services_nbi_order_form/', methods=['GET', 'POST'])
def services_nbi_order_form():
    """
    services management
    POST = create NBI service order
    """
    try:
        service_id = request.args.get('service_id')
        service_name = request.args.get('name')
        if request.method == "POST":
            # recuperer la reponse suite au POST order
            payload = render_template(
                'services_nbi/services_nbi_order_payload.html',
                externalId=request.form['externalId'],
                serviceInstanceName=request.form['serviceInstanceName'],
                customerName="generic",
                service_id=service_id,
                service_name=service_name)
            download = service.Service()
            download.download_tosca_service(service_uuid=service_id,
                                            service_name=service_name)
            response = requests.request(
                "POST",
                NBI_URL + URL_SERVICE_ORDER,
                headers=HEADERS_NBI,
                proxies=PROXIES,
                verify=False,
                data=payload)
            response = response.json()
            service_order = response
            print(service_order)
            # le template suivant affichera le resultat et on ajoutera un
            # bouton de refresh pour voir le status de l'order évoluer
            # a voir si on ne peut pas aussi afficher les VNF instance
            # et VNF modules
            return render_template(
                'services_nbi/services_nbi_order_add_result.html',
                service_order=service_order)
        # get customer list for selection
        response = requests.request(
            "GET",
            URL,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        customers = response['customer']
        return render_template(
            'services_nbi/services_nbi_order_form.html',
            service_id=service_id,
            service_name=service_name,
            customers=customers)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/service_instance_nbi_delete', methods=['GET'])
def service_instance_nbi_delete():
    """
    services management
    launch delete service instance and display page after done
    """
    try:
        # service_instance_id = request.args.get('id')
        service_instance_id = request.args.get('service_instance_id')
        service_id = request.args.get('service_id')
        customer = request.args.get('customer')
        payload_delete = render_template(
            'services_nbi/services_nbi_order_delete_payload.html',
            service_instance_id=service_instance_id,
            service_id=service_id,
            customer=customer)
        response = requests.request(
            "POST",
            NBI_URL + URL_SERVICE_ORDER,
            headers=HEADERS_NBI,
            proxies=PROXIES,
            verify=False,
            data=payload_delete)
        response = response.json()
        service_order = response
        redirect_url = ("/services_nbi/service_nbi_order_delete_result" +
                        "?service_order_id=" + service_order['id'])
        return redirect(redirect_url)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/service_nbi_order_add_result/', methods=['GET'])
def service_nbi_order_add_result():
    """
    services management
    launch delete service instance and display page after done
    """
    try:
        service_order_id = request.args.get('service_order_id')
        response = requests.request(
            "GET",
            NBI_URL + URL_SERVICE_ORDER + "/" + service_order_id,
            headers=HEADERS_NBI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        service_order = response
        return render_template(
            'services_nbi/services_nbi_order_add_result.html',
            service_order=service_order)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/service_nbi_order_delete_result', methods=['GET'])
def service_nbi_order_delete_result():
    """
    services management
    launch delete service instance and display page after done
    """
    try:
        service_order_id = request.args.get('service_order_id')
        response = requests.request(
            "GET",
            NBI_URL + URL_SERVICE_ORDER + "/" + service_order_id,
            headers=HEADERS_NBI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        service_order = response
        return render_template(
            'services_nbi/services_nbi_order_delete_result.html',
            service_order=service_order)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/services_instantiate_form/', methods=['GET', 'POST'])
def services_instantiates_form():
    """
    services management
    POST = create Service, VNFs and VF Modules from catalog
    """
    try:
        service_id = request.args.get('service_id')
        service_name = request.args.get('name')
        vnf = e2e.E2E(service_name=service_name, nbi=True)
        vnf.execute()
        vnf_message = ("New VNF " + service_name +
                       " successfully deployed.\r\n" +
                       "Service instance id: " +
                       vnf.instantiate_vnf.service_infos['instance_id'])

        render_template('instantiation/vnf_instantiation.html',
                        vnf_message=vnf_message)
        return render_template(
            'instantiation/vnf_instantiation.html',
            service_id=service_id,
            service_name=service_name,
            vnf_message=vnf_message)
    except onap_test_exceptions.NbiException:
        return render_template(
            'instantiation/vnf_instantiation.html',
            service_id=service_id,
            service_name=service_name,
            vnf_message="Creation rejected by NBI")
    except requests.exceptions.ConnectionError:
        message = 'Server error: impossible to instantiate the VNF'
        return render_template(
            'exception.html',
            message=message)
