#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
#  pylint: disable=import-error
"""
    service onboarding management
"""
import requests
from flask import Blueprint, render_template, request
import onap_tests.scenario.onboard as onboard
import onap_tests.utils.utils as onap_utils
import newportal.utils.utils as newportal_utils

# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('onboard_services', __name__, url_prefix='/onboard_services',
                static_folder='../static')
NBI_URL = newportal_utils.get_config("onap.nbi.url")
URL_SERVICE_CATALOG = newportal_utils.get_config(
    "onap.nbi.url_services_catalog")
HEADERS_NBI = newportal_utils.get_config("onap.nbi.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/onboard_services/', methods=['GET', 'POST'])
def onboard_services():
    """
    services management
    POST = onboard service
    """
    try:
        if request.method == "POST":
            # recuperer la reponse suite au POST order
            service_name = request.form['serviceName']
            service = onboard.Onboard(vendor_name="generic",
                                      service_name=service_name,
                                      customer_name="generic")
            service.onboard_vendor()
            service.onboard_vsp()
            service.onboard_vf()
            service.onboard_service()

            return render_template(
                'services/onboard_service_result.html',
                service_name=service_name)
        # get service for selection
        #
        possible_services = onap_utils.get_service_list()
        onboarded_services_full = get_services_list()
        onboarded_services = []
        for key, service in enumerate(onboarded_services_full):
            onboarded_services.append(onboarded_services_full[key]['name'])
        remaining_services = list(
            set(possible_services) -
            set(onboarded_services))
        return render_template(
            'services/onboard_service_form.html',
            services=remaining_services)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


def get_services_list():
    """
    services management
    GET = get service models via NBI
    """
    response = requests.request(
        "GET",
        NBI_URL + URL_SERVICE_CATALOG,
        headers=HEADERS_NBI,
        proxies=PROXIES,
        verify=False)
    response = response.json()

    services = response
    for service in services:
        if service['distributionStatus'] != "DISTRIBUTED":
            services.remove(service)
    return services
