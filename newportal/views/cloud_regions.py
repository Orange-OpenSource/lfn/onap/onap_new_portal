# !/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#
"""
    cloud regions management
"""
import requests
from flask import Blueprint, render_template, request
import newportal.utils.utils as newportal_utils

MOD = Blueprint('cloud_regions', __name__, url_prefix='/cloud_regions',
                static_folder='../static')
AAI_URL = newportal_utils.get_config("onap.aai.url")
URL_CLOUD_REGIONS = newportal_utils.get_config(
    "onap.aai.list_cloud_region_url")
URL_CLOUD_REGION = newportal_utils.get_config(
    "onap.aai.cloud_region_url")
URL = AAI_URL + URL_CLOUD_REGIONS

HEADERS_AAI = newportal_utils.get_config("onap.aai.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/cloud_region_list/', methods=['GET'])
def cloud_region_list():
    """
    cloud_region management
    GET = display cloud_region list
    """
    try:
        response = requests.request(
            "GET",
            URL,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        cloud_regions = response['cloud-region']
        return render_template(
            'cloud_regions/cloud_region_list.html',
            cloud_regions=cloud_regions)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/cloud_region_create_form/', methods=['GET', 'POST'])
def cloud_region_create_form():
    """
    customer management
    POST = create customer
    """
    try:
        if request.method == "POST":
            cloud_owner_name = request.form.get('CloudOwnerName')
            cloud_region_id = request.form.get('CloudRegionName')
            payload = render_template(
                'cloud_regions/cloud_region_create_payload.html',
                cloud_owner_name=cloud_owner_name,
                cloud_region_id=cloud_region_id)
            url2 = AAI_URL + URL_CLOUD_REGION + "/" + cloud_owner_name \
                   + "/" + cloud_region_id
            response = requests.request(
                "PUT",
                url2,
                headers=HEADERS_AAI,
                proxies=PROXIES,
                verify=False,
                data=payload)
            if response.status_code == 201:
                return render_template(
                    'cloud_regions/cloud_region_created.html')
            message = 'Error during creation ( Code : ' \
                      + str(response.status_code) + ' )'
            return render_template('exception.html', message=message)
        return render_template(
            'cloud_regions/cloud_region_create_form.html')
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/cloud_region_details/', methods=['GET'])
def customer_details():
    """
    customer management
    display customer details with tenants,
    service_subscriptions, services instances, service orders
    """
    try:
        cloud_region_name = request.args.get('cloud_region_name')
        cloud_owner = request.args.get('cloud_owner')
        cloudowner = cloud_owner
        cloudregionid = cloud_region_name
        full_tenant_list = []
        url_tenants = (URL + "/cloud-region/" + str(cloudowner) + "/" +
                       str(cloudregionid) + "/tenants")
        response = requests.request(
            "GET",
            url_tenants,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        list_empty = True
        if 'tenant' in response:
            list_empty = False
            tenants = response['tenant']
            for tenant in tenants:
                tenant['cloud-region-id'] = cloudregionid
                tenant['cloud-owner'] = cloudowner
                full_tenant_list.append(tenant)
        return render_template(
            'cloud_regions/cloud_region_details.html',
            cloudowner=cloud_owner,
            cloudregion=cloud_region_name,
            tenants=full_tenant_list,
            list_empty=list_empty)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception_annexe.html',
            message=message)


@MOD.route('/cloud_region_delete', methods=['GET'])
def cloud_region_delete():
    """
    cloud region management
    launch delete cloud region
    """
    try:
        cloud_owner = request.args.get('cloud_owner')
        cloud_region_id = request.args.get('cloud_region_id')
        url_cloud_region = (URL + "/cloud-region/" + str(cloud_owner) +
                            "/" + str(cloud_region_id))
        response = requests.request(
            "GET",
            url_cloud_region,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        resource_version = response['resource-version']
        url2 = (AAI_URL + URL_CLOUD_REGION + "/" + cloud_owner + \
                "/" + cloud_region_id + "?resource-version=" + resource_version)
        response = requests.request(
            "DELETE",
            url2,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        # 204 is code success with no payload
        if response.status_code == 204:
            return render_template(
                'cloud_regions/cloud_region_delete_result.html')
        message = 'Error during creation ( Code : ' \
                  + str(response.status_code) + ' )'
        return render_template('exception.html', message=message)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)
