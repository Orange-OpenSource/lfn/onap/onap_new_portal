#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
"""
    customers management
"""
import requests
from flask import Blueprint, render_template, request
import newportal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()
# pylint: disable=inconsistent-return-statements

MOD = Blueprint('customers', __name__, url_prefix='/customers',
                static_folder='../static')
AAI_URL = newportal_utils.get_config("onap.aai.url")
URL_CUSTOMERS = newportal_utils.get_config("onap.aai.list_customer_url")
URL1 = AAI_URL + URL_CUSTOMERS

URL_CUSTOMER = newportal_utils.get_config(
    "onap.aai.customer_url")

URL_CLOUD_REGIONS = newportal_utils.get_config(
    "onap.aai.list_cloud_region_url")
URL2 = AAI_URL + URL_CLOUD_REGIONS

HEADERS_AAI = newportal_utils.get_config("onap.aai.headers")

SDC_URL = newportal_utils.get_config("onap.sdc.url")
URL_SERVICES = newportal_utils.get_config("onap.sdc.url_services_in_sdc")
HEADERS_SDC = newportal_utils.get_config("onap.sdc.headers")


PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/customer_list/', methods=['GET'])
def customer_list():
    """
    menu de gestion des customers
    GET = affichage de la liste des customers
    """
    try:
        response = requests.request(
            "GET",
            URL1,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        customers = response['customer']
        return render_template(
            'customers/customer_list.html',
            customers=customers)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception_annexe.html',
            message=message)


@MOD.route('/customer_create_form/', methods=['GET', 'POST'])
def customer_create_form():
    """
    customer management
    POST = create customer
    """
    try:
        if request.method == "POST":
            customer_name = request.form.get('CustomerName')
            payload = render_template(
                'customers/customer_create_payload.html',
                customer_name=customer_name)
            url_val = AAI_URL + URL_CUSTOMER + "/" + customer_name
            response = requests.request(
                "PUT",
                url_val,
                headers=HEADERS_AAI,
                proxies=PROXIES,
                verify=False,
                data=payload)
            if response.status_code == 201:
                return render_template(
                    'customers/customer_created.html')
            message = 'Error during creation ( Code : ' \
                        + str(response.status_code) + ' )'
            return render_template('exception.html', message=message)
        return render_template(
            'customers/customer_create_form.html')
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template('exception.html', message=message)


@MOD.route('/customer_add_tenants/', methods=['GET', 'POST'])
def customer_add_tenants():
    """
    customer management
    add tenant to subscribed service
    """
    try:
        customer_name = request.args.get('customer_name')
        service = request.args.get('service')
        if request.method == "POST":
            payload = render_template(
                'customers/customer_add_tenants_payload.html',
                tenants=request.form.getlist('tenants'))
            print(payload)
            return render_template(
                'customers/customer_details.html',
                customer_name=customer_name)
        full_tenant_list = []
        response = requests.request(
            "GET",
            URL2,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        cloud_regions = response['cloud-region']
        for cloud_region in cloud_regions:
            #url_tenants
            url_val = (URL2 + "/cloud-region/" +
                       str(cloud_region['cloud-owner']) + "/" +
                       str(cloud_region['cloud-region-id']) + "/tenants")
            response = requests.request(
                "GET",
                url_val,
                headers=HEADERS_AAI,
                proxies=PROXIES,
                verify=False)
            response = response.json()
            if 'tenant' in response:
                tenants = response['tenant']
                for tenant in tenants:
                    tenant['cloud-region-id'] = cloud_region[
                        'cloud-region-id']
                    tenant['cloud-owner'] = cloud_region['cloud-owner']
                    full_tenant_list.append(tenant)
            tenant_list = []
            for tenant in full_tenant_list:
                if tenant['cloud-owner'] == "CloudOwner":
                    tenant_list.append(tenant)
                    return render_template(
                        'customers/customer_add_tenants.html',
                        tenants=tenant_list,
                        service=service,
                        customer_name=customer_name)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception_annexe.html',
            message=message)


@MOD.route('/customer_delete/', methods=['GET'])
def customer_delete():
    """
    customer management
    launch delete customer and display page after done
    """
    try:
        # customer_id = request.args.get('id')
        customer_name = request.args.get('customer_name')
        # PUT DELETE OPERATION HERE
        return render_template(
            'customers/customer_deleted.html',
            customer_name=customer_name)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception_annexe.html',
            message=message)


@MOD.route('/customer_details/', methods=['GET'])
def customer_details():
    """
    customer management
    display customer details with tenants,
    service_subscriptions, services instances, service orders
    """
    try:
        customer_name = request.args.get('customer_name')
        url_val = URL1 + '/customer/' + str(customer_name) + '?depth=all'
        response = requests.request(
            "GET",
            url_val,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        customer = response
        service = {}
        service_list = []
        if 'service-subscriptions' in customer:
            service_subscriptions = customer['service-subscriptions']\
                ['service-subscription']
            nbservice = 1
            for idx, service_subscription in enumerate(service_subscriptions):
                service[idx] = {}
                service[idx]['name'] = service_subscription['service-type']
                service[idx]['indice'] = nbservice
                service[idx]['element_tenant'] = []
                if 'relationship-list' in service_subscription:
                    tenants = service_subscription['relationship-list']\
                        ['relationship']
                    for tenant in tenants:
                        new_tenant = add_element_tenant(tenant)
                        service[idx]['element_tenant'].append(new_tenant)
                service_list.append(service[idx])
                nbservice = nbservice + 1
        return render_template(
            'customers/customer_details.html',
            customer=customer,
            service_list=service_list)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception_annexe.html',
            message=message)

def add_element_tenant(elem):
    """
    add element tenant (name, cloudowner, clourregion, tenant_id)
    """
    list_relationdata = elem['relationship-data']
    element_tenant_temp = []
    element_tenant_temp.append(elem['related-to-property'][0]['property-value'])
    for relationkeyval in list_relationdata:
        relationkey = relationkeyval['relationship-key']
        if relationkey == "cloud-region.cloud-owner":
            cloud_owner = relationkeyval['relationship-value']
            element_tenant_temp.append(cloud_owner)
        elif relationkey == "cloud-region.cloud-region-id":
            cloud_region = relationkeyval['relationship-value']
            element_tenant_temp.append(cloud_region)
        elif relationkey == "tenant.tenant-id":
            tenant_id = relationkeyval['relationship-value']
            element_tenant_temp.append(tenant_id)
    return element_tenant_temp
