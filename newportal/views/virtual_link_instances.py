#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
"""
    virtual link instances management
"""
import requests
from flask import Blueprint, render_template
import newportal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('virtual_link_instances',
                __name__, url_prefix='/virtual_link_instances',
                static_folder='../static')
AAI_URL = newportal_utils.get_config("onap.aai.url")
URL_VIRTUAL_LINK_INSTANCES = newportal_utils.get_config(
    "onap.aai.url_virtual_link_instances")
URL = AAI_URL + URL_VIRTUAL_LINK_INSTANCES
HEADERS_AAI = newportal_utils.get_config("onap.aai.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/virtual_link_instance_list/', methods=['GET'])
def virtual_link_instance_list():
    """
    vnf_instance management
    GET = display virtual_link_instance list
    """
    try:
        response = requests.request(
            "GET",
            URL,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        if 'requestError' in response:
            virtual_link_instances = {}
        else:
            virtual_link_instances = response['l3-network']
        return render_template(
            'virtual_link_instances/virtual_link_instance_list.html',
            virtual_link_instances=virtual_link_instances)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception.html',
            message=message)
