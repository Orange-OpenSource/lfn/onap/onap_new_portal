#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
"""
    services management
"""
import requests
from flask import Blueprint, render_template
import newportal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('services', __name__, url_prefix='/services',
                static_folder='../static')
SDC_URL = newportal_utils.get_config("onap.sdc.url")
URL_SERVICES = newportal_utils.get_config("onap.sdc.url_services_in_sdc")
HEADERS_SDC = newportal_utils.get_config("onap.sdc.headers")
PROXIES = newportal_utils.get_config("general.proxy")
AAI_URL = newportal_utils.get_config("onap.aai.url")
URL_AAI_SERVICES = newportal_utils.get_config("onap.aai.list_service_url")
HEADERS_AAI = newportal_utils.get_config("onap.aai.headers")


@MOD.route('/service_list/', methods=['GET'])
def service_list():
    """
    services management
    GET = display service models from SDC
    """
    try:
        response = requests.request(
            "GET",
            SDC_URL + URL_SERVICES,
            headers=HEADERS_SDC,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        services = response
        return render_template(
            'services/service_list.html',
            services=services)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception_annexe.html',
            message=message)


@MOD.route('/aai_service_list/', methods=['GET'])
def aai_service_list():
    """
    service management in AAI
    GET = display service models coming from AAI
    """
    try:
        response = requests.request(
            "GET",
            AAI_URL + URL_AAI_SERVICES,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        aai_services = response['service']
        return render_template(
            'services/aai_service_list.html',
            aai_services=aai_services)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception_annexe.html',
            message=message)
