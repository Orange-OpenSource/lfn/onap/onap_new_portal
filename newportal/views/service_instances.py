#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
#  pylint: disable=R1702
"""
    service instances management
"""
import requests
from flask import Blueprint, render_template, request, redirect
import newportal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('service_instances', __name__, url_prefix='/service_instances',
                static_folder='../static')
SO_URL = newportal_utils.get_config("onap.so.url")
URL_REQUESTS = newportal_utils.get_config(
    "onap.so.url_requests")
URL2 = SO_URL + URL_REQUESTS
HEADERS_SO = newportal_utils.get_config("onap.so.headers")
AAI_URL = newportal_utils.get_config("onap.aai.url")
URL_CUSTOMERS = newportal_utils.get_config("onap.aai.list_customer_url")
URL = AAI_URL + URL_CUSTOMERS
HEADERS = newportal_utils.get_config("onap.aai.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/service_instance_list/', methods=['GET'])
def service_instance_list():
    """
    service_instance management
    GET = display service_instance list with related customer
    """
    try:
        response = requests.request(
            "GET",
            URL,
            headers=HEADERS,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        customers = response['customer']
        service_instances = []
        for customer in customers:
            url2 = (URL + "/customer/" + customer["global-customer-id"] +
                    "?depth=all")
            response = requests.request(
                "GET",
                url2,
                headers=HEADERS,
                proxies=PROXIES,
                verify=False)
            resp = response.json()
            if 'service-subscriptions' in resp:
                services = (resp['service-subscriptions']['service-\
subscription'])
                for elem in services:
                    key1 = 'service-instances'
                    key2 = 'service-subscription'
                    if key1 in elem and 'service-instance' in elem[key1]:
                        for svc_inst in elem[key1]['service-instance']:
                            svc_inst[key2] = elem['service-type']
                            svc_inst['customer'] = resp['subscriber-name']
                            svc_inst['has_vnf_instance'] = bool('generic\
-vnf' in str(svc_inst))
                            service_instances.append(svc_inst)
        return render_template(
            'service_instances/service_instance_list.html',
            service_instances=service_instances)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception_annexe.html',
            message=message)


@MOD.route('/service_request_list/', methods=['GET'])
def service_request_list():
    """
    service_request management
    GET = display service_request list
    """
    try:
        response = requests.request(
            "GET",
            URL2,
            headers=HEADERS_SO,
            proxies=PROXIES,
            verify=False)

        response = response.json()
        so_requests = response['requestList']
        return render_template(
            'service_instances/SO_request_list.html',
            so_requests=so_requests)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception_annexe.html',
            message=message)


@MOD.route('/service_instance_delete', methods=['GET'])
def service_instance_delete():
    """
    service_instance management
    launch delete service_instance and display page after done
    """
    try:
        service_instance_id = request.args.get('service-instance-id')
        service_subscription = request.args.get('service-subscription')
        resource_version = request.args.get('resource-version')
        customer = request.args.get('customer')
        url_part1 = AAI_URL + "/aai/v13/business/customers/customer/" +\
            str(customer)
        url_part2 = "/service-subscriptions/service-subscription/" +\
            str(service_subscription)
        url_part3 = "/service-instances/service-instance/" +\
            str(service_instance_id) + "?resource-version=" +\
            str(resource_version)
        url2 = url_part1 + url_part2 + url_part3
        requests.request(
            "DELETE",
            url2,
            headers=HEADERS,
            proxies=PROXIES,
            verify=False)
        redirect_url = ("/service_instances/service_instance_delete_result")
        return redirect(redirect_url)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/service_instance_delete_result', methods=['GET'])
def service_instance_delete_result():
    """
    service_instance management
    launch delete service_instance and display page after done
    """
    try:
        return render_template(
            'service_instances/service_instance_delete_result.html')
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)
