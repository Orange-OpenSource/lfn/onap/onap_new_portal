#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
"""
    vnf instances management
"""
import requests
from flask import Blueprint, render_template, request, redirect
import newportal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('vnf_instances', __name__, url_prefix='/vnf_instances',
                static_folder='../static')
AAI_URL = newportal_utils.get_config("onap.aai.url")
URL_VNF_INSTANCES = newportal_utils.get_config("onap.aai.url_vnf_instances")
URL = AAI_URL + URL_VNF_INSTANCES
HEADERS_AAI = newportal_utils.get_config("onap.aai.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/vnf_instance_list/', methods=['GET'])
def vnf_instance_list():
    """
    vnf_instance management
    GET = display vnf_instance list with related customer
    """
    try:
        response = requests.request(
            "GET",
            URL,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)

        response = response.json()
        vnfinstancelist = response['generic-vnf']
        for vnfinstance in vnfinstancelist:
            url2 = URL +\
                "/generic-vnf/" + str(vnfinstance['vnf-id']) +\
                "/vf-modules"

            response2 = requests.request(
                "GET",
                url2,
                headers=HEADERS_AAI,
                proxies=PROXIES,
                verify=False)
            response2 = response2.json()
            vnfinstance['has_vf_module'] = bool(
                'vf-module' in response2.keys())
        return render_template(
            'vnf_instances/vnf_instance_list.html',
            vnf_instance_list=vnfinstancelist)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/vnf_instance_delete', methods=['GET'])
def vnf_instance_delete():
    """
    vnf_instance management
    launch delete vnf_instance and display page after done
    """
    try:
        vnf_instance_id = request.args.get('vnf-instance-id')
        resource_version = request.args.get('resource-version')
        url2 = AAI_URL + "/aai/v13/network/generic-vnfs/generic-vnf/" +\
                         str(vnf_instance_id) +\
                         "?resource-version=" + str(resource_version)
        print(url2)
        requests.request(
            "DELETE",
            url2,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        redirect_url = ("/vnf_instances/vnf_instance_delete_result")
        return redirect(redirect_url)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/vnf_instance_delete_result', methods=['GET'])
def vnf_instance_delete_result():
    """
    vnf_instance management
    launch delete vnf_instance and display page after done
    """
    try:
        return render_template(
            'vnf_instances/vnf_instance_delete_result.html')
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)
