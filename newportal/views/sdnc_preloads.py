#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
"""
    sdnc preloads management
"""
import json
import requests
from flask import Blueprint, render_template, request, redirect
import newportal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('sdnc_preloads', __name__, url_prefix='/sdnc_preloads',
                static_folder='../static')
SDNC_URL = newportal_utils.get_config("onap.sdnc.url")
URL_SDNC_PRELOADS = newportal_utils.get_config("onap.sdnc.url_sdnc_preloads")
HEADERS_SDNC = newportal_utils.get_config("onap.sdnc.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/sdnc_preload_list/', methods=['GET'])
def sdnc_preload_list():
    """
    menu de gestion des sdnc_preloads
    GET = affichage de la liste des sdnc_preloads
    """
    try:
        response = requests.request(
            "GET",
            SDNC_URL + URL_SDNC_PRELOADS,
            headers=HEADERS_SDNC,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        preload_list = []
        if 'preload-vnfs' in response:
            if 'vnf-preload-list' in response['preload-vnfs']:
                sdnc_preloads = response['preload-vnfs']['vnf-preload-list']
            else:
                sdnc_preloads = {}
        else:
            sdnc_preloads = {}
        for preload in sdnc_preloads:
            preload = json.dumps(preload, indent=4, sort_keys=True)
            preload_list.append(preload)
        return render_template(
            'sdnc_preloads/sdnc_preload_list.html',
            sdnc_preloads=sdnc_preloads,
            preload_list=preload_list)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception_annexe.html',
            message=message)


@MOD.route('/sdnc_preload_delete', methods=['GET'])
def sdnc_preload_delete():
    """
    sdnc_preload management
    launch delete sdnc_preload and display page after done
    """
    try:
        vnf_name = request.args.get('vnf-name')
        vnf_type = request.args.get('vnf-type')
        url_part1 = SDNC_URL + URL_SDNC_PRELOADS + "/vnf-preload-list/" +\
            str(vnf_name) + "/" + str(vnf_type)
        requests.request(
            "DELETE",
            url_part1,
            headers=HEADERS_SDNC,
            proxies=PROXIES,
            verify=False)
        redirect_url = ("/sdnc_preloads/sdnc_preload_delete_result")
        return redirect(redirect_url)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/sdnc_preload_delete_result', methods=['GET'])
def sdnc_preload_delete_result():
    """
    sdnc_preload management
    launch delete sdnc_preload and display page after done
    """
    try:
        return render_template(
            'sdnc_preloads/sdnc_preload_delete_result.html')
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message)
