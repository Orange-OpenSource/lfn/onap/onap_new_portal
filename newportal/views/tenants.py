#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
"""
    tenants management
"""
import requests
from flask import Blueprint, render_template, request
import newportal.utils.utils as newportal_utils

# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('tenants', __name__, url_prefix='/tenants',
                static_folder='../static')
AAI_URL = newportal_utils.get_config("onap.aai.url")
URL_CLOUD_REGIONS = newportal_utils.get_config(
    "onap.aai.list_cloud_region_url")
URL_CLOUD_REGION = newportal_utils.get_config(
    "onap.aai.cloud_region_url")
URL = AAI_URL + URL_CLOUD_REGIONS
HEADERS_AAI = newportal_utils.get_config("onap.aai.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/tenant_list/', methods=['GET'])
def tenant_list():
    """
    tenants management
    GET = display tenant list
    """
    try:
        full_tenant_list = []
        response = requests.request(
            "GET",
            URL,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        cloud_regions = response['cloud-region']
        for cloud_region in cloud_regions:
            cloudowner = cloud_region['cloud-owner']
            cloudregionid = cloud_region['cloud-region-id']
            url_tenants = (URL + "/cloud-region/" + str(cloudowner) + "/" +
                           str(cloudregionid) + "/tenants")
            response = requests.request(
                "GET",
                url_tenants,
                headers=HEADERS_AAI,
                proxies=PROXIES,
                verify=False)
            response = response.json()
            if 'tenant' in response:
                tenants = response['tenant']
                for tenant in tenants:
                    tenant['cloud-region-id'] = cloud_region['cloud-region-id']
                    tenant['cloud-owner'] = cloud_region['cloud-owner']
                    full_tenant_list.append(tenant)
        return render_template(
            'tenants/tenant_list.html',
            tenants=full_tenant_list)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel'
        return render_template(
            'exception.html',
            message=message)


@MOD.route('/tenant_create_form/', methods=['GET', 'POST'])
def tenant_create_form():
    """
    tenant management
    POST = create tenant for a cloud-owner and Cloud-Region
    """
    try:
        cloud_region_id = request.args.get('cloud_region_id')
        cloud_owner = request.args.get('cloud_owner')
        if request.method == "POST":
            tenant_name = request.form.get('TenantName')
            tenant_id = request.form.get('TenantID')
            payload = render_template(
                'tenants/tenant_create_payload.html',
                tenant_id=tenant_id,
                tenant_name=tenant_name)
            url2 = AAI_URL + URL_CLOUD_REGION + "/" + cloud_owner \
                   + "/" + cloud_region_id + "/tenants/tenant/" + tenant_id
            response = requests.request(
                "PUT",
                url2,
                headers=HEADERS_AAI,
                proxies=PROXIES,
                verify=False,
                data=payload)
            if response.status_code == 201:
                return render_template(
                    'tenants/tenant_created.html')
            message = 'Error during creation ( Code : ' \
                      + str(response.status_code) + ' )'
            return render_template('exception.html', message=message)
        return render_template(
            'tenants/tenant_create_form.html',
            cloudowner=cloud_owner,
            cloudregion=cloud_region_id
        )
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html',
            message=message
        )


@MOD.route('/tenant_delete', methods=['GET'])
def tenant_delete():
    """
    tenant management
    launch delete tenant
    """
    try:
        cloud_owner = request.args.get('cloud_owner')
        cloud_region_id = request.args.get('cloud_region_id')
        tenant_id = request.args.get('tenant_id')
        url_tenant = (URL + "/cloud-region/" + str(cloud_owner) + "/" +
                      str(cloud_region_id) + "/tenants/tenant/" +
                      str(tenant_id))
        response = requests.request(
            "GET",
            url_tenant,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        response = response.json()
        resource_version = response['resource-version']
        url2 = (AAI_URL + URL_CLOUD_REGION + "/" + str(cloud_owner) \
                + "/" + str(cloud_region_id) + "/tenants/tenant/" \
                + str(tenant_id) + "?resource-version=" + resource_version)
        response = requests.request(
            "DELETE",
            url2,
            headers=HEADERS_AAI,
            proxies=PROXIES,
            verify=False)
        if response.status_code == 204:
            return render_template(
                'tenants/tenant_delete_result.html')
        message = 'Error during creation ( Code : ' \
                  + str(response.status_code) + ' )'
        return render_template('exception.html', message=message)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to ONAP : check rebond tunnel ?'
        return render_template(
            'exception.html', message=message)
