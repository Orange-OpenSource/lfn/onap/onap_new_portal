#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
"""
    home screen management
"""
from flask import Blueprint, render_template

MOD = Blueprint('nbi', __name__, url_prefix='/nbi',
                static_folder='../static')


@MOD.route(
    '/nbi_menu/',
    methods=['GET'])
def nbi_menu():
    """
    menu principal
    GET = affichage de la page nbi
    """
    return render_template('nbi/nbi.html')
