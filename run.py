#!/usr/bin/env python3
# coding: utf-8
"""
    programme de gestion des services et VNF sur ONAP
"""
from newportal import MY_WEB_SERVER

MY_WEB_SERVER.run(host='0.0.0.0', port=5000, debug=True)
