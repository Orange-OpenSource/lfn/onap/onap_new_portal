# ONAP NewPortal

## Description

A Portal to manage Service, VNF, customers, Tenants using ONAP
That portal is developped in python language, using :
flask framework (that include Jinja2 templating system),
Orange Boosted (CSS/Javascript library, inspired from Bootstrap)
requests library for RestAPI toward ONAP.

## Installation

Clone the project

if you want to contribute and propose some changes please create a branch and
then begin code modification process.

go into the cloned project folder (it should be named "newportal")

## Some information

The "templates" folder contains all templates that Flask/Jinja2 will use to
generate html pages
the "static" folder contains css, images, icônes and javascript.
The content of that folder is mainly comming from Orange Boosted.

## Running the "portal"

You must have:

* python3
* pip3
* virtualenv

we supposed you are inside "newportal" folder

create and activate a virtualenv:

Unix OS

```
virtualenv portal
source portal/bin/activate
```

Windows OS:

```
virtualenv portal
portal/Scripts/activate
```

you should be now inside a python virtualenv and you can install external
libraries

```
pip install -r requirements.txt
```

If you want to be able to onboard and/or instantiate the VNFs through the new
portal, nous need to import onap-tests, a python lib developed to automate the
chaining of the ONAP operations to onboard and instantiate a solution.

First clone the repo somewhere on your environement

```
git clone https://gitlab.com/Orange-OpenSource/onap-tests.git <onap_tests_repo>
```

Then you must reference the python module in your virtualenv

Unix OS

```
cd <your_virtualenv>/lib/python3/sites-packages
ln -s  <onap_tests repo>/onap_tests onap_tests
```

Windows OS:

```
cd <your_virtualenv>\Lib\site-packages
mklink /D onap_tests <onap_tests_repo>\onap_tests
```

You can check that the lib is available by launching the following commands:

```
python
>>> import onap_tests.scenario.solution as solution
>>> deploy_ims = solution.Solution(case="ims",nbi=False)
2018-08-28 12:18:44,726 get VNF Clearwater 0 info
2018-08-28 12:18:45,999 Complete Module info for VNF Clearwater 0
>>> deploy_ims.get_info()
2018-08-28 12:18:57,080 Class to manage VNFs
2018-08-28 12:18:57,081 VNF config: {'vnf': 'ims', 'nbi': False,
'random_string': 'ZOE6UY', 'sdnc_vnf_name': 'test_ims_ZOE6UY',
'vnf_name': 'Clearwater-vIMS',
'invariant_uuid': '873d61cd-350b-430f-9e71-3b05255797d3',
'uuid': '9418a60e-dc6f-48c9-9c96-99e24ab072b5',
'Clearwater 0': {'vnf_customization_name': 'Clearwater 0',
'vnf_model_name': 'Clearwater',
'vnf_invariant_id': 'ac41d74d-293a-46f0-b64c-f5a041c12f67',
'vnf_version_id': '5c977577-0710-44ec-926f-89f34d337550',
'vnf_version': '1.0',
'vnf_customization_id': 'bcebfe0e-c547-485a-b732-2081bf2c3c75',
'vnf_type': 'clearwater0..Clearwater..base_clearwater..module-0',
'vnf_generic_name': 'Clearwater-vIMS-service-instance-ZOE6UY',
'vnf_generic_type': 'Clearwater-vIMS/Clearwater 0',
'vnf_parameters': None,
'sdnc_vnf_type': 'Clearwater..base_clearwater..module-0',
'module_invariant_id': '75ea3833-83b7-4726-b8b5-62984e805025',
'module_name_version_id': 'dad5ec28-efce-4184-9d93-ed4868eaa59a',
'module_customization_id': 'b95ab883-725f-40f1-9646-29dc7fc7b7d8',
'module_version': '1',
'module_version_id': 'dad5ec28-efce-4184-9d93-ed4868eaa59a'}}
```

Please note that newportal and onap_tests use different (but similar) config
file, a task is plan to improve that later.

Another task is planned to create a docker (it will simplify requirements and
external lib management)

## Run

we supposed you are inside "newportal" folder

```
python run.py
```

## Use

Open your favorite WebBrowser (Firefox, Chrome)
<http://@ip:5000>
5000 is the default port
